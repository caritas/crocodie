﻿namespace Crockodie
{
    using System.Collections.Generic;
    using System.Linq;

    using UnityEngine;

    public class RiverBuilder : MonoBehaviour
    {
        [SerializeField]
        private Material material;

        [SerializeField]
        [Range(0,5)]
        private float cellHeight = 1;

        [SerializeField]
        [Range(0, 5)]
        private float amplitude = 0.25f;

        [SerializeField]
        [Range(0, 5)]
        private float cellWidth = 1;

        [SerializeField]
        private  int height = 10;

        [SerializeField]
        private int offset;

        private Vector3[,] points;

        [SerializeField]
        private  int width = 10;

        private Mesh mesh;

        public void Awake()
        {
            gameObject.AddComponent<MeshFilter>();
            gameObject.AddComponent<MeshRenderer>();
            mesh = GetComponent<MeshFilter>().mesh;
            GetComponent<Renderer>().material = material;
            points = new Vector3[width + 1, height + 1];
            for (int y = 0; y < height +1 ; y++)
            {
                for (int x = 0; x < width + 1; x++)
                {
                    points[x,y] = new Vector3(x* cellWidth - width * cellWidth / 2, 0, y* cellHeight - height * cellHeight / 2);
                }
            }
            UpdateMesh();
        }

        public void Update()
        {

            for (int y = 0; y < height + 1; y++)
            {
                for (int x = 0; x < width + 1; x++)
                {
                    points[x, y].y = Mathf.Sin(Time.time+ (x+1+offset)*(y+1)) * amplitude ;
                }
            }
            UpdateMesh();
        }

        private void UpdateMesh()
        {
            var vertices = new List<Vector3>();
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    AddRectangle(vertices, x, y);
                }
            }
            mesh.vertices = vertices.ToArray();
            mesh.triangles = Enumerable.Range(0, vertices.Count).ToArray();
            mesh.RecalculateNormals();
        }

        public void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawWireCube(transform.position, new Vector3(width*cellWidth, 0.2f, height*cellHeight));
        }

        private void AddRectangle(List<Vector3> vertices, int x, int y)
        {
            //top triangle
            vertices.Add(points[x, y]);
            vertices.Add(points[x, y + 1]);
            vertices.Add(points[x + 1, y + 1]);
            //bottom triangle
            vertices.Add(points[x, y]);
            vertices.Add(points[x + 1, y + 1]);
            vertices.Add(points[x + 1, y]);
        }
    }
}