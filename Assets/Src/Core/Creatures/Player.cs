﻿namespace Crockodie.Core.Creatures
{
    using UnityEngine;

    public class Player : MonoBehaviour
    {
        [SerializeField]
        private float speed;

        [SerializeField]
        private float ownSpeed;

        [SerializeField]
        private float currentDampVelocity;

        [SerializeField]
        private float dampeningTime;

        private void Update()
        {
            this.speed = Mathf.SmoothDamp(this.speed, this.ownSpeed, ref this.currentDampVelocity, this.dampeningTime);
            this.transform.position += new Vector3(this.speed * Time.deltaTime, 0);

            if (Input.GetKeyDown(KeyCode.Space))
            {
                speed += 1;
            }
        }

        


    }
}