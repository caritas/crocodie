﻿namespace Crockodie
{
    using System;

    using JetBrains.Annotations;

    using UnityEngine;

    using Random = UnityEngine.Random;

    public class Floater : MonoBehaviour
    {
        [SerializeField]
        private float amplitude = 0.1f;

        [SerializeField]
        private float speed = 1.2f;

        private Vector3 defaultPosition;

        private int seed;

        [UsedImplicitly]
        public void Start()
        {
            defaultPosition = gameObject.transform.localPosition;
            seed = Random.Range(0, 4096);
        }

        [UsedImplicitly]
        public void Update()
        {
            var deltaY = Mathf.Sin((Time.time + seed) * speed ) * amplitude;
            var newPosition = defaultPosition;
            newPosition.y += deltaY;
            gameObject.transform.localPosition = newPosition;
        }
    }
}