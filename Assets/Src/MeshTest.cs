﻿namespace Crockodie
{
    using UnityEngine;

    using Random = System.Random;

    [RequireComponent(typeof(MeshFilter))]
    public class MeshTest : MonoBehaviour
    {
        private float[] amplitudes;

        private Mesh mesh;

        private Random random = new Random();

        private Vector3[] vertices;

        private void Start()
        {
            gameObject.AddComponent<MeshFilter>();
            gameObject.AddComponent<MeshRenderer>();
            var mesh = GetComponent<MeshFilter>().mesh;
            mesh.Clear();
            mesh.vertices = new[]
                                {
                                    new Vector3(0, 0, 0), new Vector3(0, 1, 0), new Vector3(1, 1, 0), new Vector3(1, 1, 0),
                                    new Vector3(1, 0, 1), new Vector3(0, 0, 0)
                                };
            mesh.uv = new[] { new Vector2(0, 0), new Vector2(0, 1), new Vector2(1, 1) };
            mesh.triangles = new[] { 0, 1, 2, 3, 4, 5 };
            mesh.RecalculateNormals();
        }
    }
}