﻿namespace Crockodie
{
    using UnityEngine;

    public class CameraFacing : MonoBehaviour
    {
        private void Update()
        {
            var mCamera = Camera.main;
            transform.LookAt(
                transform.position + mCamera.transform.rotation * Vector3.forward,
                mCamera.transform.rotation * Vector3.up);
        }
    }
}