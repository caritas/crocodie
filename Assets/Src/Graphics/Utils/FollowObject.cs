﻿namespace Crockodie.Graphics.Utils
{
    using UnityEngine;

    public class FollowObject : MonoBehaviour
    {
        [SerializeField]
        private GameObject goal;

        private Vector3 offset;

        private void Start()
        {
            offset = transform.position - goal.transform.position;
        }

        private void Update()
        {
            transform.position = goal.transform.position + offset;
        }
    }
}